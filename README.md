# Index

An index of my personal projects, so I can remember they exist.

Some tags help to differentiate them:
 - Some are about the programming, language, like ![](doc/cpp.png), ![](doc/asm.png), ![](doc/python.png) and ![](doc/js.png)
 - Some (maked with ![](doc/old.png)) were done when I was a teenager, and are here for sentimental and archival reasons
 - Things like ![](doc/game.png), ![](doc/web.png), ![](doc/cyber_sec.png) or ![](doc/hardware.png) mostly specify a bit more the general domain
 - ![](doc/ref.png) mark something that is meant as documentation or reference
 - ![](doc/configs.png) is mostly setup and configuration for something
 - ![](doc/crude.png) marks proofs of concept that are very incomplete and barely (if at all) functional
 - ![](doc/buggy.png) indicate projects that have crippling bugs, or an overall bad architecture


## Slightly more mature

I have a tendency of abandoning personal projects once they take more shape. But some survived the urge to start
something new, and are somewhat more 'done'. Or at least I will no longer touch them much.



**★ = Check there first :)**



| Project | Short Description | Tags |
|-----|----|----|
| ★ [HeatMapPy](https://github.com/rgcalsaverini/heatmapy)| Map visualization tool |![](doc/web.png) ![](doc/python.png) |
| ★ [Pipeline Buster](https://gitlab.com/rgcalsaverini/pipeline_buster)| A web game where you write assembly to bust a CPU's pipeline. | ![](doc/old.png) ![](doc/game.png) ![](doc/js.png) |
| [Flask Session](https://gitlab.com/rgcalsaverini/flask-session)| Flask session utilities | ![](doc/cyber_sec.png) ![](doc/python.png) |
| [Flask Kit](https://gitlab.com/rgcalsaverini/flask-kit)| A kit of useful (and slightly outdated) stuff for flask | ![](doc/python.png) ![](doc/web.png) |
| [Personal Site](https://gitlab.com/rgcalsaverini/personal-site)| A personal website on React | ![](doc/js.png) ![](doc/web.png) |
| [Labels](https://gitlab.com/rgcalsaverini/svg-labels)| API to create png labels like these on the right| ![](doc/python.png) ![](doc/web.png) |
| [PIC8 Random Box](https://gitlab.com/rgcalsaverini/pic8-random-box)| A mix of libraries for PIC 8-bit MCUs| ![](doc/cpp.png) ![](doc/hardware.png) |

## Incomplete and work in progress

Some projects are just not where I want them to be yet, but they will. Someday.

| Project | Short Description | Tags |
|-----|----|----|
| ★ [House Maker](https://gitlab.com/rgcalsaverini/house-maker)| Procedural generator of house models for 3D games | ![](doc/3d.png) ![](doc/cpp.png) ![](doc/game.png) ![](doc/crude.png)|
| ★ [SDR Car](https://gitlab.com/rgcalsaverini/sdr-car)| Interface to control a RC car with an SDR radio|![](doc/buggy.png) ![](doc/cyber_sec.png) ![](doc/cpp.png) |
| ★ [8bit computer](https://gitlab.com/rgcalsaverini/8bit_computer)| My (frequently frustrated) journey to build an 8-bit computer | ![](doc/hardware.png) ![](doc/asm.png) |
| ★ [Artillery Simulator](https://gitlab.com/rgcalsaverini/cannon-simulator)| A 3D simulator of 19th century artillery using Unreal Engine4 | ![](doc/game.png) ![](doc/3d.png) ![](doc/cpp.png) |
| ★ [Random Grammar](https://gitlab.com/rgcalsaverini/random-grammar)| Interpreter to generate random strings for procedural stuff | ![](doc/cpp.png) ![](doc/game.png) |
| ★ [Term Dash](https://gitlab.com/rgcalsaverini/term-dash)| A very simple graphing utility for the terminal | ![](doc/python.png) ![](doc/cpp.png) |
| ★ [Smart Filament Holder](https://gitlab.com/rgcalsaverini/smart-filament-holder)| A wifi 3D printer filament holder with a scale | ![](doc/cpp.png) ![](doc/hardware.png) ![](doc/3d.png)|
| [PIC ref Designs](https://gitlab.com/rgcalsaverini/pic-usb)| A bunch of PIC reference designs, software and utils | ![](doc/hardware.png) ![](doc/cpp.png) ![](doc/ref.png)|
| [Kernel](https://gitlab.com/rgcalsaverini/kernel)| A custom OS|![](doc/crude.png) ![](doc/cpp.png) ![](doc/asm.png) |
| [Reversing on Linux]()| A binary reversing reference on Linux | ![](doc/ref.png) ![](doc/cyber_sec.png) |
| [Assembly]()| A reference on assembly | ![](doc/ref.png) ![](doc/asm.png) |
| [Buffer Overflow](https://gitlab.com/rgcalsaverini/buffer-overflow)| A practice / reference for buffer overflow | ![](doc/cyber_sec.png) ![](doc/ref.png) |

## Buggy or abandoned

On some other projects, I just wanted to test one or another thing, and they were never really meant to fully work, or
parts of the code are not really something that I would be proud to show, most likely ridden with bugs.

| Project | Short Description | Tags |
|-----|----|----|
| [People Generator]()| A POC for a procedural generator of faces | ![](doc/game.png) ![](doc/python.png) ![](doc/buggy.png) |
| [Advtklndr](https://gitlab.com/rgcalsaverini/advtklndr)| Generate an Adventskalender layout for laser cutter | ![](doc/python.png) ![](doc/crude.png) |
| [Evosim](https://gitlab.com/rgcalsaverini/evolution-sim)| A dumb ecosystem sim where creatures evolve |![](doc/buggy.png) ![](doc/game.png) ![](doc/python.png) |
| [Workflow Engine](https://gitlab.com/rgcalsaverini/workflow-engine)| A POC for workflow engines | ![](doc/crude.png) ![](doc/python.png)|
| [Google Data Vis](https://gitlab.com/rgcalsaverini/google-data-vis)| A visualization of Google's data about me | ![](doc/web.png) ![](doc/crude.png) ![](doc/python.png) |

## Others

Stuff that I did a while back, that do not represent my current standards and are here for emotional reasons, or useful
configurations. And anything else that does not fit the other categories

| Project | Short Description | Tags |
|-----|----|----|
| [PSCLI](https://gitlab.com/rgcalsaverini/pscli)| A POSIX-compliant CLI parser I did 15 years ago | ![](doc/old.png) ![](doc/cpp.png) |
| [Mancala](https://gitlab.com/rgcalsaverini/mancala)| Move analysis of mancala that I did a decade ago | ![](doc/game.png) ![](doc/old.png) ![](doc/cpp.png) |
| [Ru i3](https://gitlab.com/rgcalsaverini/ru-i3)| My I3 setup | ![](doc/configs.png) |
